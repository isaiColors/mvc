<?php
class Core{
    protected $controller = 'Usuarios';
    protected $method = 'index';
    protected $parameter = [];

    public function __construct()
    {

        # controladores
        $url = $this->url();

        if (file_exists('../app/controllers/'.ucwords($url[0]).'.php')) {
            $this->controller = ucwords($url[0]);
            unset($url[0]);
        }

        require_once '../app/controllers/'.$this->controller.'.php';
        $this->controller = new $this->controller;

        # metodos
        if (isset($url[1])) {
            if (method_exists($this->controller, $url[1])) {
                $this->method = $url[1];
                unset($url[1]);
            }
        }

        # only parametros
        $this->parameter = $url ? array_values($url) : [];

        #retornar un array de parametros
        call_user_func_array([$this->controller,$this->method], $this->parameter);
    }

    public function url(){
        if (isset($_GET['url'])) {
            $url = rtrim($_GET['url'],'/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);
        }
    }
}
<?php

class Controlador{
    public function modelo($modelo){

        require_once '../app/models/'.$modelo.'.php';

        return new $modelo();
    }

    public function vista($url, $data = []){
        if (file_exists('../app/views/'.$url.'.php')) {
            require_once '../app/views/'.$url.'.php';
        }
        else{
            die('La vista no existe');
        }
    }
}
<?php
require_once 'config/config.php';

# autoload
spl_autoload_register(function ($clase){
    require_once 'lib/'.$clase.'.php';
});
?>
<?php
# base de datos
define('HOST', 'localhost');
define('USER', 'root');
define('PASS', '');
define('NAME', 'lux');

# ruta para app
define('RUTAAP', dirname(dirname(__FILE__)));

# ruta public
define('RUTAPUBLIC', 'http://localhost/git/mvclux');

# nombre del site
define('NAMEAPP', 'Luxy');
